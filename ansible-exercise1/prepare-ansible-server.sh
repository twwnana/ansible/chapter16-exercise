#!/bin/bash

echo "Script: Installing ansible and python3"
apt update -y
apt install software-properties-common -y
add-apt-repository --yes --update ppa:ansible/ansible
apt install ansible -y